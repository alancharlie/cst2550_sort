#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "sort.h"

/*
  Helper function to test if a vector is sorted
*/
bool is_sorted(std::vector<int> input)
{
  if (input.size() <= 1)
    return true;

  for (size_t i = 1; i < input.size(); ++i)
    if (input[i-1] > input[i])
      return false;
  return true;
}

TEST_CASE("merging two equal-size sorted vectors results in a sorted vector",
	  "[merge]")
{
  std::vector<int> half1 = {2,4,6,8,10};
  std::vector<int> half2 = {1,3,5,7,9};
  std::vector<int> destination(half1.size()+half2.size());

  // assert that the destination is the correct size to hold all elements
  REQUIRE(destination.size() == half1.size() + half2.size());

  // assert that both half lists are sorted
  REQUIRE(is_sorted(half1));
  REQUIRE(is_sorted(half2));

  merge(half1, half2, destination);

  SECTION("The merged vector contains both halves") {
  
    REQUIRE_THAT(destination, Catch::Matchers::Contains(half1));
    REQUIRE_THAT(destination, Catch::Matchers::Contains(half2));
    
  }

  SECTION("The merged vector is sorted") {

    REQUIRE(is_sorted(destination));
  }
}

TEST_CASE("merging an empty vector with a sorted one results in a copy of the non-empty vector", "[merge]")
{
  std::vector<int> half1 = {2,4,6,8,10};
  std::vector<int> half2;
  std::vector<int> destination(half1.size()+half2.size());
  REQUIRE(destination.size() == half1.size() + half2.size());

  // both half lists are sorted
  REQUIRE(is_sorted(half1));
  REQUIRE(is_sorted(half2));

  merge(half1, half2, destination);

  SECTION("The merged vector contains both halves") {
  
    REQUIRE_THAT(destination, Catch::Matchers::Contains(half1));
    REQUIRE_THAT(destination, Catch::Matchers::Contains(half2));
    
  }

  SECTION("The merged vector is sorted") {

    REQUIRE(is_sorted(destination));
  }

  SECTION("The merged vector is equal to first half") {
  
    REQUIRE_THAT(destination, Catch::Matchers::Equals(half1));
  }
}

TEST_CASE("merging all items from list1 then list2", "[merge]")
{
  std::vector<int> half1 = {0,1,2,3,4};
  std::vector<int> half2 = {5,6,7,8,9};
  std::vector<int> destination(half1.size()+half2.size());
  REQUIRE(destination.size() == half1.size() + half2.size());

  // both half lists are sorted
  REQUIRE(is_sorted(half1));
  REQUIRE(is_sorted(half2));

  merge(half1, half2, destination);
  SECTION("The merged vector contains both halves") {
  
    REQUIRE_THAT(destination, Catch::Matchers::Contains(half1));
    REQUIRE_THAT(destination, Catch::Matchers::Contains(half2));
    
  }

  SECTION("The merged vector is sorted") {

    REQUIRE(is_sorted(destination));
  }  

}


TEST_CASE("after calling mergesort on unsorted vector it is sorted",
	  "[mergesort]")
{
  std::vector<int> numbers = {3,5,8,2,1,0,-4,9};

  // assert that the input is initially not sorted
  REQUIRE_FALSE(is_sorted(numbers));

  mergesort(numbers);
  REQUIRE(is_sorted(numbers));
}


TEST_CASE("calling mergesort on a sorted vector results in equal vector",
	  "[mergesort]")
{
  std::vector<int> numbers = {-7, -4, -1, 0, 2, 5, 7, 9, 12};
  std::vector<int> input = numbers;

  // assert initially vectors are equal
  REQUIRE_THAT(input, Catch::Matchers::Equals(numbers));
  
  mergesort(input);

  // after sorting vectors are still equal
  REQUIRE_THAT(input, Catch::Matchers::Equals(numbers));
}

